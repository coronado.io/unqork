# Unqork

## notes
- instead of using ansible roles to deploy manifests, I'm using a chart that contains everything related to the application

## developing
- install [kind](https://kind.sigs.k8s.io/)
- install [poetry](https://python-poetry.org/docs/)

```bash

# install the dependencies
$ poetry install

# create a test cluster
$ kind create cluster

# deploy app
$ ansible-playbook -i inventory unqork.yaml -l kind -v 2>&1

# check status
$ kubectl get pods -n unqork
	NAME       READY   STATUS    RESTARTS   AGE
	unqork-0   1/1     Running   0          77s
	unqork-1   1/1     Running   0          62s
```

## assumptions
- we have privileges to deploy all required manifests and create new namespaces
- assume a kubernetes cluster is setup, and we're configuring kubectl to with the "prod-cluster" context
- assume that the gitlab runner can reach the kube api server, so open firewalls, etc